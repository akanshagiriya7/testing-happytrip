package com.skillassure.happytrip.tests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Login extends BaseClass {			
		
	/*
	 * WebDriver driver;
	 * 
	 * @BeforeTest public void setup() {
	 * System.setProperty("webdriver.chrome.driver",
	 * "F:\\Chrome_Extension\\chromedriver.exe"); driver = new ChromeDriver();
	 * 
	 * }
	 * 
	 * @AfterTest public void tearDown() { driver.close();
	 * 
	 * }
	 */
					
		@Test (dataProvider ="Login")			
		public void login(String userName, String Password)			
					
		{			
			driver.get("http://43.254.161.195:8085/happytripcrclean1/");		
			driver.manage().window().maximize();		
			driver.findElement(By.linkText("Log in as admin")).click();		
			WebElement username = driver.findElement(By.id("username"));		
			username.click();		
			username.clear();		
			username.sendKeys(userName);		
					
			WebElement password = driver.findElement(By.id("password"));		
			password.click();		
			password.clear();		
			password.sendKeys(Password);		
					
			driver.findElement(By.id("signInButton")).click();		
					
			String expected = "Schedule Flight";		
			WebElement element= driver.findElement(By.linkText(("Schedule Flight")));		
			String actual= element.getText();		
			Assert.assertEquals(actual, expected);		
					
		}			
					
					
		@DataProvider(name="Login")			
		public Object[][] testData() {			
	      				
	          return new Object[][] {				
					
				{"Admin@mindtree.com","admin"} 	
					
			};		
		}			
	}
